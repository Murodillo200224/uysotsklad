import React, { useState } from 'react'
import './navbar.scss'
import Logo from '../../img/logo-uyqur.png'
import { NavLink, Link } from 'react-router-dom'

function Navbar() {
  const [active, setActive] = useState(false)
  const NavbarClick = () => {
    setActive(!active)
  }
  return (
    <div
      className={active === false ? 'UyqurSklad_Navbar' : 'UyqurSklad_Navbar active'}
    >
      <Link to='/'>
        <div className='logo'>
          <img src={Logo} alt={Logo} />
          {active === false ? <span>Uyqur.uz</span> : null}
        </div>
      </Link>
      <span className='navbar' onClick={NavbarClick}>
        <svg
          width='12'
          height='12'
          viewBox='0 0 12 12'
          fill='none'
          xmlns='http://www.w3.org/2000/svg'
        >
          <path
            d='M7.5 9.75L3.75 6L7.5 2.25'
            stroke='#FBAA59'
            stroke-linecap='round'
            stroke-linejoin='round'
          />
        </svg>
      </span>
      <div style={{ position: 'relative', overflow: 'hidden' }}>
        <div className='UyqurSklad_Navbar_menu'>
          <NavLink to='/' exact>
            <svg
              width='28'
              height='29'
              viewBox='0 0 28 29'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                d='M23 19.6384V10.3906C23 10.2575 22.9646 10.1269 22.8974 10.012C22.8302 9.89712 22.7337 9.80219 22.6177 9.73694L14.3677 5.09632C14.2554 5.03317 14.1288 5 14 5C13.8712 5 13.7446 5.03317 13.6323 5.09632L5.3823 9.73694C5.26632 9.80219 5.16978 9.89712 5.10259 10.012C5.03541 10.1269 5 10.2575 5 10.3906V19.6384C5 19.7714 5.03541 19.9021 5.10259 20.017C5.16978 20.1319 5.26632 20.2268 5.3823 20.292L13.6323 24.9327C13.7446 24.9958 13.8712 25.029 14 25.029C14.1288 25.029 14.2554 24.9958 14.3677 24.9327L22.6177 20.292C22.7337 20.2268 22.8302 20.1319 22.8974 20.017C22.9646 19.9021 23 19.7714 23 19.6384Z'
                stroke='#A4A4A4'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
              <path
                d='M18.5958 17.3124V12.4374L9.5 7.42073'
                stroke='#A4A4A4'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
              <path
                d='M22.8966 10.0107L14.089 15.0145L5.10388 10.0098'
                stroke='#A4A4A4'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
              <path
                d='M14.089 15.0145L14.001 25.029'
                stroke='#A4A4A4'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
            </svg>
            <span>Mahsulot</span>
          </NavLink>
          <NavLink to='/transfer'>
            <svg
              width='28'
              height='28'
              viewBox='0 0 28 28'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                d='M8.625 22.5C10.0747 22.5 11.25 21.3247 11.25 19.875C11.25 18.4253 10.0747 17.25 8.625 17.25C7.17525 17.25 6 18.4253 6 19.875C6 21.3247 7.17525 22.5 8.625 22.5Z'
                stroke='#A4A4A4'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
              <path
                d='M8.625 11.25C10.0747 11.25 11.25 10.0747 11.25 8.625C11.25 7.17525 10.0747 6 8.625 6C7.17525 6 6 7.17525 6 8.625C6 10.0747 7.17525 11.25 8.625 11.25Z'
                stroke='#A4A4A4'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
              <path
                d='M8.625 11.25V17.25'
                stroke='#A4A4A4'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
              <path
                d='M19.8752 22.5C21.325 22.5 22.5002 21.3247 22.5002 19.875C22.5002 18.4253 21.325 17.25 19.8752 17.25C18.4255 17.25 17.2502 18.4253 17.2502 19.875C17.2502 21.3247 18.4255 22.5 19.8752 22.5Z'
                stroke='#A4A4A4'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
              <path
                d='M19.8752 17.25L19.8751 13.4889C19.875 12.2954 19.4009 11.1509 18.5571 10.3071L15 6.75'
                stroke='#A4A4A4'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
              <path
                d='M15 10.5V6.75H18.75'
                stroke='#A4A4A4'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
            </svg>
            <span>Transfer</span>
          </NavLink>
          <NavLink to='kirim-chiqim'>
            <svg
              width='28'
              height='28'
              viewBox='0 0 28 28'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                d='M20.3752 22.5C21.825 22.5 23.0002 21.3247 23.0002 19.875C23.0002 18.4253 21.825 17.25 20.3752 17.25C18.9255 17.25 17.7502 18.4253 17.7502 19.875C17.7502 21.3247 18.9255 22.5 20.3752 22.5Z'
                stroke='#A4A4A4'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
              <path
                d='M20.3752 17.25L20.3751 13.4889C20.375 12.2954 19.9009 11.1509 19.0571 10.3071L15.5 6.75'
                stroke='#A4A4A4'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
              <path
                d='M15.5 10.5V6.75H19.25'
                stroke='#A4A4A4'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
              <path
                d='M7.625 11.25C9.07475 11.25 10.25 10.0747 10.25 8.625C10.25 7.17525 9.07475 6 7.625 6C6.17525 6 5 7.17525 5 8.625C5 10.0747 6.17525 11.25 7.625 11.25Z'
                stroke='#A4A4A4'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
              <path
                d='M7.625 11.25L7.62515 15.0111C7.6252 16.2046 8.0993 17.3491 8.94317 18.1929L12.5002 21.75'
                stroke='#A4A4A4'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
              <path
                d='M12.5002 18V21.75H8.75018'
                stroke='#A4A4A4'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
            </svg>
            <span>Kirim-chiqim</span>
          </NavLink>
        </div>
      </div>
    </div>
  )
}

export default Navbar
