import React from 'react'
import './mahsulot.scss'
import { Input, Button, Table, Select } from 'antd'

const { Option } = Select

const columns = [
  {
    title: 'NOMI',
    dataIndex: 'name',
    // width: 200,
    render: (text: string) => <p>{text}</p>,
  },
  {
    title: 'TURI',
    dataIndex: 'turi',
    // width: 110,
  },
  {
    title: 'MIQDOR',
    dataIndex: 'miqdori',
    // width: 110,
  },
  {
    title: 'BIRLIK',
    dataIndex: 'birlik',
    // width: 110,
  },
]

const columns2 = [
  {
    title: 'NOMI',
    dataIndex: 'name',
    width: 200,
    key: 'name',
    render: (text: string) => <p>{text.slice(0, 20)} ...</p>,
  },
  {
    title: 'MIQDOR',
    dataIndex: 'miqdori',
    key: 'miqdori',

    render: (text: string) => (
      <Input value={text} style={{ borderRadius: '5px', textAlign: 'center' }} />
    ),
  },
  { title: 'BIRLIK', dataIndex: 'birlik', key: 'birlik' },
  {
    title: '',
    dataIndex: '',
    key: 'x',
    width: 50,
    render: () => (
      <span style={{ cursor: 'pointer' }}>
        <svg
          width='16'
          height='16'
          viewBox='0 0 16 16'
          fill='none'
          xmlns='http://www.w3.org/2000/svg'
        >
          <path
            d='M12.5 3.5L3.5 12.5'
            stroke='#ED4646'
            stroke-linecap='round'
            stroke-linejoin='round'
          />
          <path
            d='M12.5 12.5L3.5 3.5'
            stroke='#ED4646'
            stroke-linecap='round'
            stroke-linejoin='round'
          />
        </svg>
      </span>
    ),
  },
]

const data = [
  {
    key: 1,
    name: 'Lorem ipsum dolor sit amet, con adipiscing elit. ',
    turi: 'Armatura',
    miqdori: 1000,
    birlik: 'tonna',
  },
]
for (let i = 0; i < 10; i++) {
  data.push({
    key: i,
    name: `Lorem ipsum dolor sit amet, con adipiscing elit.${i}`,
    turi: 'Armatura',
    miqdori: 1000 + i,
    birlik: `tonna ${i}`,
  })
}

const SearchIcon = () => {
  return (
    <svg
      width='16'
      height='16'
      viewBox='0 0 16 16'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M7.25 12.5C10.1495 12.5 12.5 10.1495 12.5 7.25C12.5 4.35051 10.1495 2 7.25 2C4.35051 2 2 4.35051 2 7.25C2 10.1495 4.35051 12.5 7.25 12.5Z'
        stroke='#FFA953'
        stroke-linecap='round'
        stroke-linejoin='round'
      />
      <path
        d='M10.9622 10.9626L13.9998 14.0001'
        stroke='#FFA953'
        stroke-linecap='round'
        stroke-linejoin='round'
      />
    </svg>
  )
}

function Mahsulot() {
  return (
    <div className='UyqurSklad_mahsulot'>
      <div>
        <div>
          <Input
            placeholder='Mahsulot nomi, turi, birligi bilan qidirish'
            style={{ width: 'calc(100% - 204px)', borderRadius: '5px' }}
            suffix={<SearchIcon />}
          />
          <Button style={{ borderRadius: '5px' }}>
            <svg
              width='17'
              height='16'
              viewBox='0 0 17 16'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                d='M4.5 8H12.5'
                stroke='#555555'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
              <path
                d='M2 5H15'
                stroke='#555555'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
              <path
                d='M7 11H10'
                stroke='#555555'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
            </svg>
            Filtr
          </Button>
          <Button style={{ borderRadius: '5px' }}>
            <svg
              width='17'
              height='16'
              viewBox='0 0 17 16'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                d='M5.25 7C6.49264 7 7.5 5.99264 7.5 4.75C7.5 3.50736 6.49264 2.5 5.25 2.5C4.00736 2.5 3 3.50736 3 4.75C3 5.99264 4.00736 7 5.25 7Z'
                stroke='#555555'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
              <path
                d='M11.75 7C12.9926 7 14 5.99264 14 4.75C14 3.50736 12.9926 2.5 11.75 2.5C10.5074 2.5 9.5 3.50736 9.5 4.75C9.5 5.99264 10.5074 7 11.75 7Z'
                stroke='#555555'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
              <path
                d='M5.25 13.5C6.49264 13.5 7.5 12.4926 7.5 11.25C7.5 10.0074 6.49264 9 5.25 9C4.00736 9 3 10.0074 3 11.25C3 12.4926 4.00736 13.5 5.25 13.5Z'
                stroke='#555555'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
              <path
                d='M11.75 9.5V13'
                stroke='#555555'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
              <path
                d='M13.5 11.25H10'
                stroke='#555555'
                stroke-linecap='round'
                stroke-linejoin='round'
              />
            </svg>
            Kiritish
          </Button>
        </div>
        <Table
          style={{ marginTop: '10px', width: '99%' }}
          columns={columns}
          dataSource={data}
          pagination={{
            pageSize: 50,
            position: ['bottomLeft'],
          }}
          onChange={(pagination, filters, sorter, extra) => {
            console.log('params', pagination, filters, sorter, extra)
          }}
          // scroll={{ y: 'calc(100vh - 250px)' }}
        />
      </div>
      <div>
        <div>
          <p>Sklad partya</p>
          <Table
            style={{ marginTop: '10px', width: '99%' }}
            columns={columns2}
            dataSource={data}
            pagination={false}
          />
        </div>
        <div>
          <Select
            placeholder='Tanlang'
            style={{ width: '100%', marginRight: 6 }}
            suffixIcon={
              <svg
                width='10'
                height='6'
                viewBox='0 0 10 6'
                fill='none'
                xmlns='http://www.w3.org/2000/svg'
              >
                <path d='M0.5 0.5L5 5.5L9.5 0.5H0.5Z' fill='#2C3F45' />
              </svg>
            }
          >
            <Option value='jack'>Jack</Option>
            <Option value='lucy'>Lucy</Option>
            <Option value='Yiminghe'>yiminghe</Option>
          </Select>
          <Button
            style={{
              padding: '0 32px',
              height: '34px',
              background: '#FBAA59',
              border: 'none',
              borderRadius: '5px',
            }}
            type='primary'
            danger
          >
            Saqlash
          </Button>
        </div>
      </div>
    </div>
  )
}

export default Mahsulot
