import React from 'react'
import { Button, Input, Table } from 'antd'
import './transfer.scss'
const SearchIcon = () => {
  return (
    <svg
      width='16'
      height='16'
      viewBox='0 0 16 16'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M7.25 12.5C10.1495 12.5 12.5 10.1495 12.5 7.25C12.5 4.35051 10.1495 2 7.25 2C4.35051 2 2 4.35051 2 7.25C2 10.1495 4.35051 12.5 7.25 12.5Z'
        stroke='#FFA953'
        stroke-linecap='round'
        stroke-linejoin='round'
      />
      <path
        d='M10.9622 10.9626L13.9998 14.0001'
        stroke='#FFA953'
        stroke-linecap='round'
        stroke-linejoin='round'
      />
    </svg>
  )
}

const columns = [
  {
    title: 'SANA',
    dataIndex: 'sana',
    render: (text: string) => <p>{text}</p>,
  },
  {
    title: 'STATUS',
    dataIndex: 'status',
  },
  {
    title: 'HOLATI',
    dataIndex: 'holat',
  },
  {
    title: 'HODIM',
    dataIndex: 'hodim',
  },
]

const data = [
  {
    key: 1,
    sana: '15.02.2021   15:00:10 ',
    status: 'TASDIQLANGAN',
    holat: '1 000',
    hodim: 'Erniyazov Laziz Shavkat o’g’li',
  },
]
for (let i = 0; i < 10; i++) {
  data.push({
    key: i,
    sana: `15.02.2021   15:00:1${i}`,
    status: 'TASDIQLANGAN',
    holat: `1 000 ${i}`,
    hodim: `Erniyazov Laziz Shavkat o’g’li`,
  })
}

function Transfer() {
  return (
    <div className='UyqurSklad_transfer'>
      <div>
        <Input
          placeholder='Mahsulot nomi, turi, birligi bilan qidirish'
          style={{ width: 'calc(100% - 90px)', borderRadius: '5px' }}
          suffix={<SearchIcon />}
        />
        <Button style={{ borderRadius: '5px' }}>
          <svg
            width='17'
            height='16'
            viewBox='0 0 17 16'
            fill='none'
            xmlns='http://www.w3.org/2000/svg'
          >
            <path
              d='M4.5 8H12.5'
              stroke='#555555'
              stroke-linecap='round'
              stroke-linejoin='round'
            />
            <path
              d='M2 5H15'
              stroke='#555555'
              stroke-linecap='round'
              stroke-linejoin='round'
            />
            <path
              d='M7 11H10'
              stroke='#555555'
              stroke-linecap='round'
              stroke-linejoin='round'
            />
          </svg>
          Filtr
        </Button>
      </div>
      <Table
        style={{ marginTop: '10px', width: '99%' }}
        columns={columns}
        dataSource={data}
        pagination={{
          pageSize: 50,
          position: ['bottomLeft'],
        }}
        // scroll={{ y: 'calc(100vh - 250px)' }}
      />
    </div>
  )
}

export default Transfer
