import React from 'react'
import { Switch, Route } from 'react-router-dom'
import './container.scss'
import KirimChiqim from './KirimChiqim/kirimchiqim'
import Mahsulot from './Mahsulot/mahsulot'
import Transfer from './Transfer/transfer'

function Container() {
  return (
    <div className='UyqurSklad_Container'>
      <Switch>
        <Route path='/' exact>
          <Mahsulot />
        </Route>
        <Route path='/transfer' exact>
          <Transfer />
        </Route>
        <Route path='/kirim-chiqim' exact>
          <KirimChiqim />
        </Route>
      </Switch>
    </div>
  )
}

export default Container
