import React from 'react'
import './App.scss'
import 'antd/dist/antd.css'
import Container from './components/Container/container'
import Menu from './components/Menu/menu'
import Navbar from './components/Navbar/navbar'

function App() {
  return (
    <div className='UyqurSklad'>
      <div className='UyqurSklad_col1'>
        <Navbar />
      </div>
      <div className='UyqurSklad_col2'>
        <Menu />
        <Container />
      </div>
    </div>
  )
}

export default App
